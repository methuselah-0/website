;;; Copyright © 2017, 2019, 2020 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This file is copied from the GNU Guix web site.
;;;
;;; Run 'guix build -f .guix.scm' to build the web site.

(use-modules (guix) (gnu)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (guix modules)
             (guix git-download)
             (guix gexp)
             (guix inferior)
             (srfi srfi-1)
             (srfi srfi-9)
             (ice-9 match))

(define this-directory
  (dirname (current-filename)))

(define source
  (local-file this-directory "my-web-site"
              #:recursive? #t
              #:select? (git-predicate this-directory)))

(define (package+propagated-inputs package)
  (match (package-transitive-propagated-inputs package)
    (((labels packages) ...)
     (cons package packages))))

(define build
  ;; We need Guile-JSON for 'packages-json-builder'.
    (with-imported-modules (source-module-closure
                            '((guix build utils)))
      #~(begin
          (use-modules (guix build utils)
                       (ice-9 popen)
                       (ice-9 match))

          (setvbuf (current-output-port) 'line)
          (setvbuf (current-error-port) 'line)

          (setenv "GUILE_LOAD_PATH"
                  (string-join (list #$guile-syntax-highlight)
                               (string-append
                                "/share/guile/site/"
                                (effective-version) ":")
                               'suffix))
          (setenv "GUILE_LOAD_COMPILED_PATH"
                  (string-join (list #$guile-syntax-highlight)
                               (string-append
                                "/lib/guile/"
                                (effective-version)
                                "/site-ccache:")
                               'suffix))
          (copy-recursively #$source ".")

          ;; So we can read/write UTF-8 files.
          (setenv "GUIX_LOCPATH"
                  #+(file-append (specification->package "glibc-utf8-locales")
                                 "/lib/locale"))
          (setenv "LC_ALL" "en_US.utf8")

          ;; Use a sane default.
          (setenv "XDG_CACHE_HOME" "/tmp/.cache")

          (format #t "Running 'haunt build'...~%")
          (invoke #+(file-append haunt "/bin/haunt")
                  "build")

          (mkdir-p #$output)
          (copy-recursively "/tmp/website" #$output
                            #:log (%make-void-port "w")))))

(computed-file "my-web-site" build
               #:guile (specification->package "guile")
               #:options '(#:effective-version "3.0"))
