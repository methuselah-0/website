This blog is licensed under a [Creative Commons Attribution-ShareAlike 4.0
International License](https://creativecommons.org/licenses/by-sa/4.0/).

The website structure and theme are based on David Thomson
[blog](https://dthompson.us).
